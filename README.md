# README

## Create an user
To create a new user access to /create_user in your browser (e.g: http://localhost:3000/create_user)

## View details for the given user
To view detailf for a user with id :id and token :token, you should call the endpoint /users/:id?token=:token (e.g: http://localhost:3000/users/:id?token=:token)
