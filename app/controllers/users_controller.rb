class UsersController < ApplicationController
  require 'jwt'
  before_action :set_user, only: [:show, :update, :destroy]
  before_action :validate_token, only: [:show]

  # GET /users
  def index
    @users = User.all

    render json: @users, except: [:token]
  end

  def create_user_form
    #   render json: {}
      render "form"
  end

  # GET /users/1
  def show
      render json: @user
  end

  # POST /users
  def create
    @user = User.new(user_params)
    payload = { :email => @user[:email] }
    @user[:token] = JWT.encode payload, Rails.application.config.token_secret, 'none'

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:email, :first_name, :last_name, :zipcode, :street, :country)
    end

    # Only allow requests with a valid token
    def validate_token
        params.require(:token)
        raise ArgumentError, "Invalid token" if @user[:token] != params[:token]
    end
end
