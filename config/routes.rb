Rails.application.routes.draw do
  # get 'form', to: :show, controller: 'users'
  resources :users
  get 'create_user', action: :create_user_form, controller: 'users'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
